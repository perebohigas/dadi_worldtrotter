//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Peter on 21.12.18.
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var textField: UITextField!
    @IBOutlet var celsiusLabel: UILabel!
    
    var fahrenheitValue: Measurement<UnitTemperature>? {
        didSet {
            updateCelsiusLabel()
        }
    }
    var celsiusValue: Measurement<UnitTemperature>? {
        if let fahrenheitValue = fahrenheitValue {
            return fahrenheitValue.converted(to: .celsius)
        } else {
            return nil
        }
    }
    var firstTime: Bool = true

    let validCharacters: CharacterSet = CharacterSet.init(charactersIn: ".").union(CharacterSet.decimalDigits)
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("ConversionViewController loaded its view.")
        updateCelsiusLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ConversionViewController shows its view.")
        if firstTime {
            firstTime = false
            setBackgroundColor(newBackgroundColor: getColorBasedTimeDay())
            print("Set background color according to the time of day")
        } else {
            setBackgroundColor(newBackgroundColor: getRandomColor())
            print("Set a random color as background color")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let enteredStringInCharacterSet = CharacterSet(charactersIn: string)
        let hasOnlyValidCharacters = validCharacters.isSuperset(of: enteredStringInCharacterSet)
        
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")
        
        if !hasOnlyValidCharacters {
            return false
        } else {
            if existingTextHasDecimalSeparator != nil, replacementTextHasDecimalSeparator != nil {
                return false
            } else {
                return true
            }
        }
    }
    
    @IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField) {
        if let text = textField.text, let value = Double(text) {
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit)
        } else {
            fahrenheitValue = nil
        }
    }
    
    func updateCelsiusLabel() {
        if let celsiusValue = celsiusValue {
            celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        } else {
            celsiusLabel.text = "???"
        }
    }
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }

    func setBackgroundColor(newBackgroundColor: UIColor) {
        self.view.backgroundColor = newBackgroundColor
    }
    
    func getColorBasedTimeDay() -> UIColor {
        let currentHour = Calendar.current.component(.hour, from: Date())
        var colorBasedOnTime:UIColor
        
        switch currentHour {
        case 6..<12:
            colorBasedOnTime = UIColor(hue: 0.1556, saturation: 0.33, brightness: 0.96, alpha: 1.0)
        case 12..<14:
            colorBasedOnTime = UIColor(hue: 36/360, saturation: 45/100, brightness: 86/100, alpha: 1.0)
        case 14..<16:
            colorBasedOnTime = UIColor(red: 0.8471, green: 0.5804, blue: 0.4902, alpha: 1.0)
        case 18..<22:
            colorBasedOnTime = UIColor(red: 72/255, green: 145/255, blue: 84/255, alpha: 1.0)
        case 22..<23:
            colorBasedOnTime = UIColor(red: 0.2549, green: 0.3333, blue: 0.5098, alpha: 1)
        case 0..<6:
            colorBasedOnTime = UIColor.darkGray
        default:
            colorBasedOnTime = UIColor.white
        }
        
        return colorBasedOnTime
    }
    
    func getRandomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    func getRandomColor() -> UIColor {
        return UIColor(red:   getRandomFloat(),
                       green: getRandomFloat(),
                       blue:  getRandomFloat(),
                       alpha: 1.0)
    }
    
}
